package com.epam.sms.sender;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    private static final String ACCOUNT_SID = "ACb54023d6cb0c6d839291b17fc7770dde";
    private static final String AUTH_TOKEN = "69e74a484529300e75a187e5d1051d11";

    public static void send(String str){
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380936821638"),
                        new PhoneNumber("+12017132246"), str).create();
    }
}
