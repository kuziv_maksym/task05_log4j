package com.epam;

import com.epam.mathematic.DivNumber;
import org.apache.logging.log4j.*;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class Application {

    public static Logger logger = LogManager.getLogger(Application.class);


    public static void main(String[] args) {
        DivNumber divNumber = new DivNumber();
        divNumber.Div(5,0);
        logger.trace("Trace");
        logger.warn("Warn");
        logger.fatal("Fatal");
        logger.error("Error");
    }
}
